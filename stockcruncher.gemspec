# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'stockcruncher/version'

dev_deps = {
  'bundler' => '~> 1.17',
  'rspec' => '~> 3.9',
  'rake' => '~> 11.3',
  'rubocop' => '0.90',
  'webmock' => '~> 2.0.3',
  'simplecov' => '~> 0.18'
}

deps = {
  'thor' => '~> 0.20'
}

Gem::Specification.new do |s|
  s.name = 'stockcruncher'
  s.version = StockCruncher::VERSION
  s.authors = ['Richard Delaplace']
  s.email = 'r.delaplace@free.fr'
  s.license = 'Apache-2.0'

  s.summary = 'Stock market data cruncher.'
  s.description = 'StockCruncher is a simple tool to crunch stock market data.'
  s.homepage = 'https://gitlab.com/yue-ruby/stockcruncher'

  s.files = `git ls-files`.lines.map(&:chomp)
  s.bindir = 'bin'
  s.executables = `git ls-files bin/*`.lines.map do |exe|
    File.basename(exe.chomp)
  end
  s.require_paths = ['lib']

  s.required_ruby_version = '>= 2.4.0'

  dev_deps.each_pair do |deps_gem, deps_version|
    s.add_development_dependency deps_gem, deps_version
  end

  deps.each_pair do |deps_gem, deps_version|
    s.add_dependency deps_gem, deps_version
  end
end
