#!/usr/bin/ruby
# frozen_string_literal: true

require 'simplecov'
SimpleCov.start

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'stockcruncher'
require 'webmock/rspec'

# Force expect syntax over should
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

dir = File.dirname(__FILE__)
# Load helpers
Dir[File.join(dir, 'helpers', '*.rb')].sort.each { |file| require file }

def now
  Time.new.strftime('%Y-%m-%dT%H:%M:%S%z').insert(-3, ':')
end

# Load stubs
files = File.join(dir, 'stockcruncher', 'stubs', '*.rb')
Dir[files].sort.each { |file| require file }

# To test the cli
def start(caller_object, subcommand = nil)
  cmd = caller_object.class.description.split(' ')
  args = [subcommand, *cmd].compact
  StockCruncher.start(args)
end
