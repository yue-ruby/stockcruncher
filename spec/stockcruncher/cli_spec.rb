# frozen_string_literal: true

require 'json'
require 'spec_helper'

daily = File.read('spec/files/SYM.daily')
quote = File.read('spec/files/SYM.quote')

describe StockCruncher::CLI do # rubocop:disable Metrics/BlockLength
  context 'version' do
    it 'prints the version.' do
      out = "StockCruncher version #{StockCruncher::VERSION}\n"
      expect { start(self) }.to output(out).to_stdout
    end
  end

  context 'daily NODATA -c spec/files/stockcruncher.yml' do
    it 'Should not get any data and should fail.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context 'daily SYM -c spec/files/stockcruncher.yml' do
    it 'Get the daily time serie for SYM.' do
      expect { start(self) }.to output(daily).to_stdout
    end
  end

  context 'daily SYM -u -c spec/files/stockcruncher.yml' do
    it 'Get the daily time serie for SYM.' do
      expect { start(self) }.to output(daily).to_stdout
    end
  end

  context 'movingaverages SYM -c spec/files/stockcruncher.yml' do
    it 'Get the daily time serie for SYM.' do
      expect { start(self) }.to output('').to_stdout
    end
  end

  context 'movingaverages SYM -u -c spec/files/stockcruncher.yml' do
    it 'Get the daily time serie for SYM.' do
      expect { start(self) }.to output('').to_stdout
    end
  end

  context 'quote NODATA -c spec/files/stockcruncher.yml' do
    it 'Should not get any data and should fail.' do
      expect { start(self) }.to raise_error(SystemExit)
    end
  end

  context 'quote SYM -c spec/files/stockcruncher.yml' do
    it 'Get the quote for SYM.' do
      expect { start(self) }.to output(quote).to_stdout
    end
  end

  context 'notexistingcmd' do
    it 'is an unknown command and does nothing.' do
      out = "Could not find command \"notexistingcmd\".\n"
      expect { start(self) }.to output(out).to_stderr
    end
  end
end
