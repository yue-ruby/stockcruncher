# frozen_string_literal: true

require 'spec_helper'

quote = 'symbol,open,high,low,price,volume,latestDay,previousClose,change,ch' \
        "angePercent\r\nSYM,100.0000,100.1000,99.9000,100.0000,4,2020-07-30," \
        "100.0000,0.0000,0.0000%\r\n"
q_err = '{}'
daily = "timestamp,open,high,low,close,volume\r\n2020-08-03,23.8500,24.6500," \
        "23.7400,24.5500,3112972\r\n2020-07-31,24.0100,24.5100,23.5900,23.81" \
        "00,5482485\r\n2020-07-30,24.4700,24.6600,23.5200,23.6600,6466738\r" \
        "\n2020-07-29,24.7500,25.0300,24.0400,24.5600,4605804\r\n2020-07-28," \
        "25.9900,26.0700,24.5100,24.7500,6904261\r\n"
d_err = "{\n    \"Error Message\": \"Invalid API call.\"\n}"
i_ema = "q=SELECT ema200 FROM ema WHERE symbol = 'SYM' ORDER BY time DESC"
maval = '{"results":[{"statement_id":0,"series":[{"name":"ema","columns":[' \
        '"time","ema200"],"values":[["2017-' \
        '03-01T18:00:00Z",1]]}]}]}'
mvavg = '{"results":[{"statement_id":0,"series":[{"name":"daily","columns":[' \
        '"time","close","change","changePercent","volume"],"values":[["2017-' \
        '03-01T18:00:00Z",1,0,0,1],["2017-03-02T18:00:00Z",1,0,0,1],["2017-0' \
        '3-03T18:00:00Z",1,0,0,1],["2017-03-04T18:00:00Z",1,0,0,1],["2017-03' \
        '-05T18:00:00Z",1,0,0,1],["2017-03-06T18:00:00Z",1,0,0,1]]}]}]}'

RSpec.configure do |config|
  config.before(:each) do
    # requests an API without extra arguments
    stub_request(:get, 'https://www.alphavantage.co/query?' \
      'function=GLOBAL_QUOTE&symbol=NODATA&apikey=demo&datatype=csv')
      .to_return('status' => 200, 'body' => q_err, 'headers' => {})
    stub_request(:get, 'https://www.alphavantage.co/query?' \
      'function=GLOBAL_QUOTE&symbol=SYM&apikey=demo&datatype=csv')
      .to_return('status' => 200, 'body' => quote, 'headers' => {})
    stub_request(:get, 'https://www.alphavantage.co/query?' \
      'function=TIME_SERIES_DAILY&symbol=NODATA&apikey=demo' \
      '&datatype=csv&outputsize=compact')
      .to_return('status' => 200, 'body' => d_err, 'headers' => {})
    stub_request(:get, 'https://www.alphavantage.co/query?' \
      'function=TIME_SERIES_DAILY&symbol=SYM&apikey=demo' \
      '&datatype=csv&outputsize=compact')
      .to_return('status' => 200, 'body' => daily, 'headers' => {})
    stub_request(:post, 'http://localhost:8086/query?db=test')
      .with(body: i_ema)
      .to_return('status' => 204, 'body' => maval, 'headers' => {})
    stub_request(:post, 'http://localhost:8086/query?db=test')
      .to_return('status' => 204, 'body' => mvavg, 'headers' => {})
    stub_request(:post, 'http://localhost:8086/write?db=test')
      .to_return('status' => 204, 'body' => '', 'headers' => {})
  end
end
