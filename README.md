# StockCruncher
[![License][license-img]](LICENSE)
[![build status][status-img]][master-commit]
[![coverage report][cov-img]][pages]
[![Gem Version][gem-img]][gem]

1. [Overview](#overview)
1. [Description](#description)
1. [Setup](#setup)
1. [Usage](#usage)
1. [Environment variables](#environment-variables)
1. [Examples](#examples)
1. [Limitations](#limitations)
1. [Development](#development)
1. [Miscellaneous](#miscellaneous)

## Overview

`StockCruncher` is a stock market data cruncher.

## Description

This tool requests a stock market data API and parse the incoming data in order
to write the data in a database for later calculation and use.

## Setup

    $ gem install stockcruncher
    $ mkdir /etc/stockcruncher && cd /etc/stockcruncher
    $ echo 'AlphaVantage:' > stockcruncher.yml
    $ echo '  apikey: CHANGEME' >> stockcruncher.yml
    $ echo 'InfluxDB:' >> stockcruncher.yml
    $ echo '  scheme: http' >> stockcruncher.yml
    $ echo '  host: localhost' >> stockcruncher.yml
    $ echo '  port: 8086' >> stockcruncher.yml
    $ echo '  user: CHANGEMEAGAIN' >> stockcruncher.yml
    $ echo '  password: CHANGEMETOO' >> stockcruncher.yml
    $ echo '  dbname: stock' >> stockcruncher.yml

## Usage

An interactive help is available with:

    $ stockcruncher help [subcommand]

## Environment variables

Parameters in /etc/stockcruncher/stockcrunucher.yml can be overloaded with
environment variables.
Environment variables should match SCR_<COMPONENTID>_<ITEM>.
Component ID are as follow.
- AlphaVantage: AV
- InfluxDB: IDB
Items are upcase keys. See template /etc/stockcruncher/stockcrunucher.yml for
reference.

## Examples

To get daily time serie data of a symbol:

    $ stockcruncher daily AAPL

To get last day endpoint data of a symbol:

    $ stockcruncher quote AAPL

To override a parameter with environment variable:

    $ SCR_IDB_HOST=192.168.0.80; stockcruncher quote AAPL

## Limitations

Data are currently scraped from AlphaVantage API.
More source could be great especially because AlphaVantage doesn't provide EU
intraday data.
InfluxDB is used as database to keep time series data.

## Development

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

[license-img]: https://img.shields.io/badge/license-Apache-blue.svg
[status-img]: https://gitlab.com/yue-ruby/stockcruncher/badges/devel/pipeline.svg
[master-commit]: https://gitlab.com/yue-ruby/stockcruncher/commits/master
[cov-img]: https://gitlab.com/yue-ruby/stockcruncher/badges/devel/coverage.svg
[pages]: https://yue-ruby.gitlab.io/stockcruncher/
[gem-img]: https://badge.fury.io/rb/stockcruncher.svg
[gem]: https://badge.fury.io/rb/stockcruncher
