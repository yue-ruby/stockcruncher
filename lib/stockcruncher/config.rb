#!/usr/bin/ruby
# frozen_string_literal: true

require 'yaml'

module StockCruncher
  # this is a module to load configuration file and environment variable
  module Config
    module_function

    # Load config file and override with env variables
    def load(file)
      config = YAML.load_file(file)
      overload_alphavantage(config)
      overload_influxdb(config)
    end

    def overload(config, prefix, component, items)
      items.each do |key|
        var = "#{prefix}#{key.upcase}"
        config[component][key] = ENV[var] unless ENV[var].nil?
      end
      config
    end

    def overload_alphavantage(config)
      prefix = 'SCR_AV_'
      component = 'AlphaVantage'
      items = %w[apikey]
      overload(config, prefix, component, items)
    end

    def overload_influxdb(config)
      prefix = 'SCR_IDB_'
      component = 'InfluxDB'
      items = %w[scheme host port user password dbname]
      overload(config, prefix, component, items)
    end
  end
end
