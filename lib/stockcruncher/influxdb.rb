#!/usr/bin/ruby
# frozen_string_literal: true

require 'date'
require 'json'
require 'net/http'

module StockCruncher
  # this is a class to write time series to database
  class InfluxDB
    # Class constructor method
    def initialize(config, insecure = false)
      @cfg = config[self.class.name.split('::').last]
      @insecure = insecure
    end

    def get_daily_values(symbol, fullsize)
      values = %w[close change changePercent volume]
      data = query('daily', symbol, values, fullsize)
      data['columns'].zip(data['values'].transpose).to_h
    end

    def get_ma_values(symbol, fullsize)
      values = %w[ema200]
      data = query('ema', symbol, values, fullsize)
      data['columns'].zip(data['values'].transpose).to_h
    end

    # Method to calculate moving averages based on last day values
    def moving_averages(symbol, fullsize, catchup)
      data = get_daily_values(symbol, fullsize)
      mas = catchup ? get_ma_values(symbol, fullsize) : { 'time' => [] }
      tags = create_tags(symbol)
      dates, series, weights = data.values_at 'time', 'close', 'volume'
      series.each_index do |i|
        next if mas['time'].include? dates[i]

        write_moving_averages(tags, series[i, 201], weights[i, 201], dates[i])
        break unless fullsize
      end
    end

    # Method to create tags hash containing only symbol
    def create_tags(symbol)
      { 'symbol' => symbol }
    end

    # Method to export historical data to database
    def export_history(symbol, timeseries, catchup)
      tags = create_tags(symbol)
      if catchup
        series = get_daily_values(symbol, true)
        series['time'].each { |date| timeseries.delete(date) }
      end
      timeseries.each_pair do |date, values|
        write('daily', tags, values, date)
      end
    end

    # Method to export latest data to database
    def export_last_day(values)
      tags = create_tags(values.delete('symbol'))
      date = values.delete('latestDay')
      write('daily', tags, values, date)
    end

    # Method to format and array of values into comma separated string
    def format_values(values)
      values.map { |k, v| "#{k}=#{v}" }.join(',')
    end

    # Method to calculate all statistics
    def write_moving_averages(tags, serie, weights, date)
      write('ema', tags, StockCruncher::Stats.list_ema(serie), date)
      write('lwma', tags, StockCruncher::Stats.list_lwma(serie), date)
      write('sma', tags, StockCruncher::Stats.list_sma(serie), date)
      write('vwma', tags, StockCruncher::Stats.list_vwma(serie, weights), date)
    end

    # Method to query data in bucket
    def query(name, symbol, values, full)
      url = "#{@cfg['scheme']}://#{@cfg['host']}:#{@cfg['port']}/query?" \
            "db=#{@cfg['dbname']}"
      size = full ? '' : 'LIMIT 201'
      body = "q=SELECT #{values.join(',')} FROM #{name} " \
             "WHERE symbol = '#{symbol}' ORDER BY time DESC #{size}"
      data = JSON.parse(request(url, body).body)['results'][0]['series']
      raise StandardError, 'No data' if data.nil?

      data[0]
    end

    # Method to send http post request
    def request(url, body)
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = uri.scheme.eql?('https')
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE if @insecure
      req = Net::HTTP::Post.new(uri.request_uri)
      req.basic_auth(@cfg['user'], @cfg['password'])
      req.body = body
      http.request(req)
    end

    # Method to write data in bucket
    def write(name, tags, values, date)
      url = "#{@cfg['scheme']}://#{@cfg['host']}:#{@cfg['port']}/write?" \
            "db=#{@cfg['dbname']}"
      timestamp = DateTime.parse("#{date}T18:00:00").strftime('%s%N')
      body = "#{name},#{format_values(tags)} #{format_values(values)} " \
             "#{timestamp}"
      request(url, body)
    end
  end
end
