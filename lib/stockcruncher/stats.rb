#!/usr/bin/ruby
# frozen_string_literal: true

module StockCruncher
  # this is a module with various statistic calculation methods
  module Stats
    extend self

    RANGES = [5, 10, 20, 30, 50, 100, 200].freeze

    # Calculate multiple range of exponential moving average
    def list_ema(values)
      h = {}
      RANGES.each do |n|
        next if values.size < n + 1

        h["ema#{n}"] = ema(values[0, n + 1])
      end
      h
    end

    # Calculate multiple range of linearly weighted moving average
    def list_lwma(values)
      h = {}
      RANGES.each do |n|
        next if values.size < n

        weights = (1..n).to_a.reverse
        h["lwma#{n}"] = sma(values[0, n], weights)
      end
      h
    end

    # Calculate multiple range of simple moving average
    def list_sma(values)
      h = {}
      RANGES.each do |n|
        next if values.size < n

        h["sma#{n}"] = sma(values[0, n])
      end
      h
    end

    # Calculate multiple range of volume weighted moving average
    def list_vwma(values, volumes)
      h = {}
      RANGES.each do |n|
        next if values.size < n

        h["vwma#{n}"] = sma(values[0, n], volumes[0, n])
      end
      h
    end

    private

    # Calculate exponential moving average
    def ema(array, factor = 2, weights = nil)
      f = factor.to_f / array.size
      n = array.size - 1
      tsma = sma(array[0, n], weights)
      ysma = sma(array[1, n], weights)
      (tsma * f + ysma * (1 - f)).round(4)
    end

    # Calculate simple moving average
    def sma(array, weights = nil)
      factor = weights.nil? ? Array.new(array.size, 1) : weights
      dividend = array.each_with_index.map { |v, i| v * factor[i] }
      (dividend.sum.to_f / factor.sum).round(4)
    end
  end
end
