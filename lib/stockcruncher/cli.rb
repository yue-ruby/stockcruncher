#!/usr/bin/ruby
# frozen_string_literal: true

require 'json'
require 'thor'
require 'yaml'

module StockCruncher
  # Simple CLI for StockCruncher
  class CLI < Thor
    class_option(
      :config,
      aliases: ['-c'],
      type: :string,
      default: '/etc/stockcruncher/stockcruncher.yml',
      desc: 'Yaml formatted config file to load ' \
            '(default to /etc/stockcruncher/stockcruncher.yml).'
    )
    class_option(
      :insecure,
      aliases: ['-k'],
      type: :boolean,
      default: false,
      desc: 'Ignore SSL certificate (default to false).'
    )
    class_option(
      :quiet,
      aliases: ['-q'],
      type: :boolean,
      default: false,
      desc: 'Run silently (default to false).'
    )

    desc 'version', 'Print stockcruncher current version'
    def version
      puts "StockCruncher version #{StockCruncher::VERSION}"
    end

    desc('daily SYMBOL [options]',
         'Crunch SYMBOL stock market data for daily time series.')
    option(
      :full,
      aliases: ['-f'],
      type: :boolean,
      default: false,
      desc: 'Full data size.'
    )
    option(
      :catchup,
      aliases: ['-u'],
      type: :boolean,
      default: false,
      desc: 'Catch up the missing data only.'
    )
    def daily(symbol)
      opts = options.dup
      config = YAML.load_file(opts['config'])
      cruncher = StockCruncher::AlphaVantage.new(config, opts['insecure'])
      data = cruncher.crunch_daily(symbol, opts['full'])
      influx = StockCruncher::InfluxDB.new(config)
      influx.export_history(symbol, data, opts['catchup'])
      puts JSON.pretty_generate(data) unless opts['quiet']
    end

    desc('movingaverages SYMBOL [options]',
         'Calculate and export moving averages for requested symbol.')
    option(
      :all,
      aliases: ['-a'],
      type: :boolean,
      default: false,
      desc: 'Recalculate all MA historical values.'
    )
    option(
      :catchup,
      aliases: ['-u'],
      type: :boolean,
      default: false,
      desc: 'Catch up the missing data only.'
    )
    def movingaverages(symbol)
      opts = options.dup
      config = YAML.load_file(opts['config'])
      influx = StockCruncher::InfluxDB.new(config)
      influx.moving_averages(symbol, opts['all'], opts['catchup'])
    end

    desc('quote SYMBOL [options]',
         'Crunch SYMBOL stock market data for last day quote.')
    def quote(symbol)
      opts = options.dup
      config = StockCruncher::Config.load(opts['config'])
      cruncher = StockCruncher::AlphaVantage.new(config, opts['insecure'])
      data = cruncher.crunch_quote(symbol)
      influx = StockCruncher::InfluxDB.new(config)
      influx.export_last_day(data)
      puts JSON.pretty_generate(data) unless opts['quiet']
    end
  end
end
