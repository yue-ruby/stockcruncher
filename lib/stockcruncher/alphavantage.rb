#!/usr/bin/ruby
# frozen_string_literal: true

require File.join(File.dirname(__FILE__), 'cruncher.rb')

module StockCruncher
  # This is an data cruncher class for AlphaVantage API.
  class AlphaVantage < Cruncher
    API_URL = 'https://www.alphavantage.co/query?'

    # Method to calculate missing data (previousClose, change, changePercent)
    def calculate_missing_data(hash)
      keys = hash.keys
      hash.each_with_index do |(date, v), index|
        prevday = keys[index + 1]
        next if prevday.nil?

        prevclose = hash[prevday]['close']
        hash[date] = v.merge(generate_missing_data(v['close'], prevclose))
      end
      hash
    end

    # Method to calculate change difference
    def change(value, base)
      (value - base).round(4).to_s
    end

    # Method to calculate percentage of change
    def change_percent(value, base)
      ((value / base - 1) * 100).round(4).to_s
    end

    # Method to create a new hash from two arrays of keys and values
    def create_hash(descriptions, values)
      descriptions.split(',').zip(values.split(',')).to_h
    end

    # Main method to crunch data.
    def crunch_daily(symbol, fullsize)
      url = API_URL + parameters(symbol, 'TIME_SERIES_DAILY')
      url += "&datatype=csv&outputsize=#{fullsize ? 'full' : 'compact'}"
      res = request(url)
      transform_daily(res.body)
    end

    # Main method to crunch data.
    def crunch_quote(symbol)
      url = API_URL + parameters(symbol, 'GLOBAL_QUOTE')
      url += '&datatype=csv'
      res = request(url)
      transform_quote(res.body)
    end

    # Method to generate missing data
    def generate_missing_data(current, previous)
      {
        'previousClose' => previous,
        'change' => change(current.to_f, previous.to_f),
        'changePercent' => change_percent(current.to_f, previous.to_f)
      }
    end

    # Set parameters of api call
    def parameters(symbol, serie)
      p = "function=#{serie}"
      p += "&symbol=#{symbol}"
      p += "&apikey=#{@config[self.class.name.split('::').last]['apikey']}"
      p
    end

    # Method to transform raw data to constructed hash
    def prepare_daily_timeserie(data)
      lines = data.split("\r\n")
      desc = lines.shift.split(',').drop(1)
      hash = {}
      lines.each do |line|
        values = line.split(',')
        date = values.shift
        hash[date] = desc.zip(values).to_h
      end
      hash
    end

    # Method to transform daily result to nested hash
    def transform_daily(rawdata)
      raise StandardError, 'No data' if rawdata.match?(/Error Message/)

      values = prepare_daily_timeserie(rawdata)
      calculate_missing_data(values)
    end

    # Method to transform quote result to hash
    def transform_quote(rawdata)
      raise StandardError, 'No data' if rawdata.match?(/{}/)

      values = create_hash(*rawdata.split("\r\n"))
      values['close'] = values.delete('price')
      values['changePercent'] = values['changePercent'].delete('%')
      values
    end
  end
end
