#!/usr/bin/ruby
# frozen_string_literal: true

require 'net/http'

module StockCruncher
  # This is an data cruncher abstract class.
  class Cruncher
    # Class constructor method
    def initialize(config, insecure = false)
      @config = config
      @insecure = insecure
    end

    # Method to send http get request
    def request(url)
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = uri.scheme.eql?('https')
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE if @insecure
      req = Net::HTTP::Get.new(uri.request_uri)
      http.request(req)
    end
  end
end
