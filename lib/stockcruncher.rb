#!/usr/bin/ruby
# frozen_string_literal: true

# StockCruncher class
module StockCruncher
  class << self
    Dir[File.join(File.dirname(__FILE__), '*', '*.rb')].sort.each do |file|
      require file
    end

    def start(args = ARGV)
      StockCruncher::CLI.start(args)
    rescue StandardError => e
      warn e.message
      exit 1
    end
  end
end
